#GRAFICO TUTTI I DATI+MEDIA 1min
import matplotlib.pyplot as plt
import seaborn as sns
from readData import readAllData, readCleanData
from avgFunctions import avg

def grADM1m(df, requiredVar):
    df = readAllData()
    for i in requiredVar: 
        m1m = avg(df, i,'60s')
        sns.set_style('whitegrid')
        #print(m1m.index()) 
        print(df.index())
        df.plot(x= 'TIME', y= i, kind= 'line')
        m1m.plot(x= 'TIME', y= i , kind= 'line')
        plt.title('All Data and 1min Avg ' + i +'/TIME') 
    #printaindicegrafico
    plt.show()