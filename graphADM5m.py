#GRAFICO TUTTI I DATI+MEDIA 5min
import matplotlib.pyplot as plt
import seaborn as sns
from readData import readAllData, readCleanData
from avgFunctions import avg

def grADM5m(df, requiredVar):
    df = readAllData()
    for i in requiredVar: 
        m5m = avg(df, i,'300s')
        sns.set_style('whitegrid')
        df.plot(x= 'TIME', y= i, kind= 'line')
        m5m.plot(x= 'TIME', y= i , kind= 'line')
        plt.title('All Data and 5min Avg ' + i +'/TIME') 
    plt.show()