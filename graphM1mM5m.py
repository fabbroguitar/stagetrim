#GRAFICO MEDIA 1,5min
import matplotlib.pyplot as plt
import seaborn as sns
from readData import readAllData, readCleanData
from avgFunctions import avg

def grM1mM5m(df, requiredVar):
    for i in requiredVar: 
        m1m = avg(df, i,'60s')
        m5m = avg(df, i,'300s')
        sns.set_style('whitegrid')
        m1m.plot(x= 'TIME', y= i, kind= 'line')
        m5m.plot(x= 'TIME', y= i , kind= 'line')
        plt.title('Avg 1min + avg 5min ' + i +'/TIME') 
        plt.show()