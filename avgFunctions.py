#FILE CON LE FUNZIONI CALCOLO MEDIA
import math
import numpy as np
import pandas as pd
from datetime import datetime, timedelta 

#lista pezzi da 60sec
def getChunks(df, minutes):
    td = timedelta(minutes = minutes)
    firstTime = df.TIME[0].replace(second = 0, microsecond = 0)
    lastTime = df.TIME[len(df)-1]
    
    chunkList = []
# confronto indici e reset nuovo indice avanti
    stepTime = firstTime
    firstTime = firstTime - td
    while stepTime < lastTime:
        chunk = df[np.logical_and(df.TIME >= firstTime, df.TIME < stepTime)]
        chunk = chunk.reset_index(level=0, drop=True)
        if len(chunk) > 0 :
            chunkList.append(chunk)
        firstTime = stepTime
        stepTime = stepTime + td
    return chunkList

#MEDIA ANGOLI (GRADI-RADIANTI-ARCTANGENTE-MODULO)
def avgAngle(arrayAngle): 
    sumcos = 0 
    sumsin = 0 
    for i in range (0, len(arrayAngle)): 
        rad = math.radians(arrayAngle[i])
        cos = math.cos(rad)
        sin = math.sin(rad) 
        sumcos = sumcos + cos 
        sumsin = sumsin + sin 
    arcotang = math.atan2(sumsin, sumcos)
    degree = math.degrees(arcotang) 
    return (degree + 360) % 360 
    

def avgAnglFromChunk(chunk, colName):
    time0 = chunk.TIME.tolist()[0]
    arrayAngle = chunk[colName].tolist()
    medang = avgAngle(arrayAngle)
    return time0, medang

#MEDIA if TIPO DI DATO then UTILIZZA MEDIA ADATTA ALLA COLONNA 
def avg(df, col, intTime):
    med = 0
    if col == 'TWS' or col == 'AWS' or col == 'SOG' :
        med = df.set_index('TIME').resample(intTime)[col].mean()
        ###print(med) ###
    else:
        
        minutes = intTime
        if minutes == '60s':
            minutes = 1
        elif minutes == '300s':
            minutes = 5
        elif minutes == '600s':
            minutes = 10
        elif minutes == '1800s':
            minutes = 30 
          
        timeAndCol = df[['TIME', col]]
        chunkList = getChunks(timeAndCol, minutes)

        arrayTime = []
        arrayAvgAngl = []

        for chunk in chunkList:
            time, avg = avgAnglFromChunk(chunk, col)
            arrayTime.append(time)
            arrayAvgAngl.append(avg)
        #med = pd.DataFrame({'TIME': arrayTime, col: arrayAvgAngl})
        med = pd.DataFrame(data={'TIME': arrayTime, col: arrayAvgAngl})
        ##print(med)  ###
        
    return med