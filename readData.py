#FILE CHE PRENDE I DATI DAL CSV
import pandas as pd
import datetime as dt

def readAllData():
    # CARICA DATI DA FILE.CSV TENENDO DATE,TIME,LAT,LON COME STRINGHE PER CONSERVARE GLI 0 INIZIALI
    df = pd.read_csv('010918.csv', converters={'DATE': lambda x: str(x),'TIME': lambda x: str(x)})
    df["THR_FLG"], df["CHKSM"] = zip(*df["THR_FLG*CHKSM"].str.split('*').tolist()) 
    del df["THR_FLG*CHKSM"]
    df['THR_FLG'] = (df['THR_FLG']).astype(int)
    df = df.drop(df.columns[[0,16,17,19]], axis=1)
    # FORMATTAZIONE DATA YYYY-MM-DD
    #df.TIME = df.TIME.str[0:8]   # 010918,053644.40
    DD = df.DATE.str[0:2]       
    MM = df.DATE.str[2:4]
    YY = df.DATE.str[4:6]
    df['DATE'] = str('20') + YY + MM + DD + str(' ') + df['TIME']
    df['DATE'] = pd.to_datetime(df['DATE'])
    del df['TIME']
    df.rename(columns={'DATE': 'TIME'}, inplace=True)
    return df

def readCleanData ():
    df = readAllData()
    #RIMOZIONE ERRORI GPS LAT=0 E SENSORI THR_FLA>0
    df = df[df.THR_FLG == 0]
    df = df[df.LAT != 0]
    df = df.reset_index(level=0, drop=True)
    return df  