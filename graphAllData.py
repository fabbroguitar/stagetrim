#GRAFICO CON TUTTI I DATI
import matplotlib.pyplot as plt
import seaborn as sns
from readData import readAllData, readCleanData

def grAD(df, requiredVar):
    df = readAllData()
    for i in requiredVar: 
        sns.set_style('whitegrid') 
        df.plot(x='TIME', y=i, kind='line')
        plt.title('All Data ' + i +'/TIME') 
    plt.show()