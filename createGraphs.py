#FILE CHE CREA IL GRAFICO SELEZIONATI
from readData import readAllData, readCleanData
from graphAllData import grAD
from graphADM1m import grADM1m
from graphADM5m import grADM5m
from graphM1mM5m import grM1mM5m
from graphM5mM10mM30m import grM5mM10mM30m

def createGr(requiredVar, withError, selGraph):
    df = 0
    if withError:       #tipo dati
        df = readAllData()
    else:
        df = readCleanData()
        if selGraph == 'All Data':  # op1 all data
            grAD(df, requiredVar)
    if selGraph == 'All Data + avg 1min':   #opz 2 alldata + med1min
        grADM1m(df, requiredVar)
    if selGraph == 'All Data + avg 5min':   #opz 3 alldata + med5min
        grADM5m(df, requiredVar)
    if selGraph == 'Avg 1min + avg 5min':   #opz 4 Avg 1min + avg 5min
        grM1mM5m(df, requiredVar)
    if selGraph == 'Avg 5min + avg 10min + avg 30min':  #opz5 vg 5min + avg 10min + avg 30min    
        grM5mM10mM30m(df, requiredVar)