#INPUT TERMINALE
def getBoolChoice(question):
    strinput = input(question + ' ')
    strinput = strinput.lower()
    if strinput == 'si' or strinput == 'y' or strinput == 'yes':
        return True
    return False

def makeQuestions(varList):
    returnlist = []
    for s in varList:
        if getBoolChoice('Show ' + s + '?'):
            returnlist.append(s)
    return returnlist

def getSingleOpt(options):
    c = 1
    for s in options:
       print(str(c) + ': ' + s)
       c = c + 1
    val = input('Choose one: ')
    try:
        val = int(val)
    except ValueError:
        val = 1
    return options[val-1]   