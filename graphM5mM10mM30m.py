#GRAFICO MEDIA 5,10,30min
import matplotlib.pyplot as plt
import seaborn as sns
from readData import readAllData, readCleanData
from avgFunctions import avg

def grM5mM10mM30m(df, requiredVar):
    for i in requiredVar: 
        m5m = avg(df, i,'300s')
        m10m = avg(df, i, '600s')
        m30m = avg(df, i,'1800s')
        sns.set_style('whitegrid')
        m5m.plot(x= 'TIME', y= i, kind= 'line')
        m10m.plot(x= 'TIME', y= i , kind= 'line')
        m30m.plot(x= 'TIME', y= i , kind= 'line')
        plt.title('Avg 5min + avg 10min + avg 30min ' + i +'/TIME') 
        plt.show()