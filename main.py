#FILE PRINCIPALE DA ESEGUIRE NEL TERMINALE PYTHON
from inputFunctions import getBoolChoice, makeQuestions, getSingleOpt
from createGraphs import createGr

variabili = ['TWS' ,     #truewindspeed
            'TWD' ,      #truewinddirection  
            'TW_MAG' ,   #truewindmagnetic
            'AWS' ,      #apparentwindspeed
            'AWA' ,      #apparentwindangle
            'SOG' ,      #speedoverground
            'ROT' ,      #rateoftourn
            'HEADING']   #direzione

#OPZIONI SELEZIONE GRAFICO
curve = ['All Data',
         'All Data + avg 1min',
         'All Data + avg 5min',
         'Avg 1min + avg 5min',
         'Avg 5min + avg 10min + avg 30min']
#IN
requiredVar = makeQuestions(variabili)
print()
withError = getBoolChoice('Do you want to keep the errors?')
print()
selGraph = getSingleOpt(curve)
print()
#OUT
createGr(requiredVar, withError, selGraph)